#! /bin/bash
files=(4 8 12 16 20 24 28 32 36 40)

for i in "${files[@]}"
do
	file="pmed$i"
	echo "!! Rodando o GLPSOL com o arquivo dat/$file.dat"
	echo "## RELATORIO GLPSOL LFSCHAUREN/RDLUGOKENSKI PARA $file.dat ##" > glpsol-reports/$file.txt
	for j in {1..5}
	do
   		echo "!! Rodada $j do $file. ++ Inicio: `date`"
   		echo "!! Rodada $j do $file. ++ Inicio: `date`" >> glpsol-reports/$file.txt
   		glpsol --model p-median.mod --data dat/$file.dat --tmlim 1800 >> glpsol-reports/$file.txt
	done
done