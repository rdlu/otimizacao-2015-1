#! /usr/bin/env ruby
# Por Luis Fernando Schauren, Rodrigo Dlugokenski
# Otimizacao Combinatoria 2015-1 - Prof Marcus Ritt
# Ciencia da Computacao - Instituto de Informatica da UFRGS

$stderr.puts 'Pmediana Otimizacao 2015-1'
$stderr.puts 'Por Luis Fernando Schauren / Rodrigo Dlugokenski'
$stderr.puts "Usando #{RUBY_DESCRIPTION}"
DEBUG=false

require './pmed_data'
require './pmed_state'
require './iterated_local_search'
require 'optparse'
require 'set'

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: ruby run-sa.rb [options]"
  opts.on('-a', '--arquivo NAME', 'Arquivo de entrada [OBRIGATORIO]') { |v| options[:file] = v }
  opts.on('-n', '--divisor NUM', 'Divisor do criterio de parada (nro de arestas/NUM) [padrao 1/4]') { |v| options[:divisor] = v.to_i }
  opts.on('-m', '--melhora NUM', 'Numero de iteracoes sem melhora [padrao 10]') { |v| options[:max_improv] = v.to_i }
  opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
    options[:verbose] = v
  end

  opts.on_tail("-h", "--help", "Show this message") do
    puts opts
    exit
  end

end.parse!

if(options[:file])
  data = PmedData.new(options[:file])
  estado = PmedState.new(data)
  divisor = options[:divisor] ? options[:divisor] : 4
  max_improv = options[:max_improv] ? options[:max_improv] : 10
  ils = IteratedLocalSearch.new(estado,estado.edge_count/divisor,max_improv)
  puts '>>>>>>>>> ILS Iniciado <<<<<<<<<'
  t1 = Time.now
  busca = ils.search
  t2 = Time.now
  puts "Solucao final com custo #{busca.total_cost}"
  puts "P Medianas escolhidas  #{busca.p_medianas.to_a}"
  delta = t2 - t1
  puts "Tempo total real: #{delta} secs"
else
  puts "Usage: ruby run-sa.rb [options]"
  puts "    -a, --arquivo NAME               Arquivo de entrada [OBRIGATORIO]"
  puts "    -d, --debug                      Imprime mensagens de debug"
  puts "    -n, --divisor NUM   Divisor do criterio de parada (nro de arestas/NUM) [padrao 1/4]"
  puts "    -m, --melhora NUM   Numero de iteracoes sem melhora [padrao 10]"

end
