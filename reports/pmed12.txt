## RELATORIO ILS LFSCHAUREN/RDLUGOKENSKI PARA pmed12.txt ##
!! Rodada 1 do pmed12. ++ Inicio: Sáb Jun 20 00:29:58 BRT 2015
Lendo arquivo data/pmed12.txt.
Arquivo lido com sucesso. Vertices: 300, P: 10
Solucao Inicial: 8825
>>>>>>>>> ILS Iniciado <<<<<<<<<
Total de iteracoes 36
Solucao final com custo 6634
P Medianas escolhidas  [216, 43, 2, 138, 83, 293, 168, 173, 5, 171]
Tempo total real: 7.703199708 secs
!! Rodada 2 do pmed12. ++ Inicio: Sáb Jun 20 00:30:21 BRT 2015
Lendo arquivo data/pmed12.txt.
Arquivo lido com sucesso. Vertices: 300, P: 10
Solucao Inicial: 8956
>>>>>>>>> ILS Iniciado <<<<<<<<<
Total de iteracoes 42
Solucao final com custo 6634
P Medianas escolhidas  [171, 173, 216, 2, 138, 293, 83, 168, 5, 43]
Tempo total real: 9.083961181 secs
!! Rodada 3 do pmed12. ++ Inicio: Sáb Jun 20 00:30:46 BRT 2015
Lendo arquivo data/pmed12.txt.
Arquivo lido com sucesso. Vertices: 300, P: 10
Solucao Inicial: 8578
>>>>>>>>> ILS Iniciado <<<<<<<<<
Total de iteracoes 46
Solucao final com custo 6634
P Medianas escolhidas  [138, 171, 168, 173, 5, 43, 216, 293, 83, 2]
Tempo total real: 10.066856895 secs
!! Rodada 4 do pmed12. ++ Inicio: Sáb Jun 20 00:31:11 BRT 2015
Lendo arquivo data/pmed12.txt.
Arquivo lido com sucesso. Vertices: 300, P: 10
Solucao Inicial: 8838
>>>>>>>>> ILS Iniciado <<<<<<<<<
Total de iteracoes 37
Solucao final com custo 6634
P Medianas escolhidas  [293, 43, 171, 5, 216, 138, 168, 2, 173, 83]
Tempo total real: 8.141829317 secs
!! Rodada 5 do pmed12. ++ Inicio: Sáb Jun 20 00:31:35 BRT 2015
Lendo arquivo data/pmed12.txt.
Arquivo lido com sucesso. Vertices: 300, P: 10
Solucao Inicial: 9309
>>>>>>>>> ILS Iniciado <<<<<<<<<
Total de iteracoes 49
Solucao final com custo 6634
P Medianas escolhidas  [293, 83, 173, 216, 5, 43, 138, 2, 171, 168]
Tempo total real: 10.754972189 secs
