## RELATORIO ILS LFSCHAUREN/RDLUGOKENSKI PARA pmed16.txt ##
!! Rodada 1 do pmed16. ++ Inicio: Sáb Jun 20 00:32:01 BRT 2015
Lendo arquivo data/pmed16.txt.
Arquivo lido com sucesso. Vertices: 400, P: 5
Solucao Inicial: 10135
>>>>>>>>> ILS Iniciado <<<<<<<<<
Total de iteracoes 25
Solucao final com custo 8162
P Medianas escolhidas  [266, 373, 228, 378, 19]
Tempo total real: 9.364033255 secs
!! Rodada 2 do pmed16. ++ Inicio: Sáb Jun 20 00:32:46 BRT 2015
Lendo arquivo data/pmed16.txt.
Arquivo lido com sucesso. Vertices: 400, P: 5
Solucao Inicial: 9683
>>>>>>>>> ILS Iniciado <<<<<<<<<
Total de iteracoes 38
Solucao final com custo 8165
P Medianas escolhidas  [266, 29, 194, 378, 373]
Tempo total real: 14.231096311 secs
!! Rodada 3 do pmed16. ++ Inicio: Sáb Jun 20 00:33:37 BRT 2015
Lendo arquivo data/pmed16.txt.
Arquivo lido com sucesso. Vertices: 400, P: 5
Solucao Inicial: 9653
>>>>>>>>> ILS Iniciado <<<<<<<<<
Total de iteracoes 37
Solucao final com custo 8183
P Medianas escolhidas  [35, 173, 29, 58, 228]
Tempo total real: 13.867174197 secs
!! Rodada 4 do pmed16. ++ Inicio: Sáb Jun 20 00:34:28 BRT 2015
Lendo arquivo data/pmed16.txt.
Arquivo lido com sucesso. Vertices: 400, P: 5
Solucao Inicial: 8857
>>>>>>>>> ILS Iniciado <<<<<<<<<
Total de iteracoes 14
Solucao final com custo 8183
P Medianas escolhidas  [58, 35, 29, 173, 228]
Tempo total real: 5.134374597 secs
!! Rodada 5 do pmed16. ++ Inicio: Sáb Jun 20 00:35:10 BRT 2015
Lendo arquivo data/pmed16.txt.
Arquivo lido com sucesso. Vertices: 400, P: 5
Solucao Inicial: 10618
>>>>>>>>> ILS Iniciado <<<<<<<<<
Total de iteracoes 24
Solucao final com custo 8162
P Medianas escolhidas  [228, 373, 266, 378, 19]
Tempo total real: 8.955019207 secs
