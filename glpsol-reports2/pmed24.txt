## RELATORIO GLPSOL LFSCHAUREN/RDLUGOKENSKI PARA pmed24.dat ##
!! Rodada 1 do pmed24. ++ Inicio: Dom Jun 21 14:56:44 BRT 2015
GLPSOL: GLPK LP/MIP Solver, v4.52
Parameter(s) specified in the command line:
 --model p-median.mod --data dat/pmed24.dat --tmlim 7200
Reading model section from p-median.mod...
35 lines were read
Reading data section from dat/pmed24.dat...
1011 lines were read
Generating Total_Distance...
Generating P...
Generating Correct_Attendance...
Generating Correct_Attendance_2...
Model has been successfully generated
GLPK Integer Optimizer, v4.52
250502 rows, 250500 columns, 1000000 non-zeros
250500 integer variables, all of which are binary
Preprocessing...
250501 rows, 250500 columns, 750500 non-zeros
250500 integer variables, all of which are binary
Scaling...
 A: min|aij| =  1.000e+00  max|aij| =  1.000e+00  ratio =  1.000e+00
Problem data seem to be well scaled
Constructing initial basis...
Size of triangular part is 250501
Solving LP relaxation...
GLPK Simplex Optimizer, v4.52
250501 rows, 250500 columns, 750500 non-zeros
      0: obj =   3.162200000e+04  infeas =  9.900e+01 (0)
*    99: obj =   3.162200000e+04  infeas =  0.000e+00 (0)
*   500: obj =   3.145000000e+04  infeas =  0.000e+00 (0)
*  1000: obj =   3.145000000e+04  infeas =  0.000e+00 (0)
*  1500: obj =   3.145000000e+04  infeas =  0.000e+00 (0)
*  2000: obj =   3.145000000e+04  infeas =  0.000e+00 (0)
*  2500: obj =   3.145000000e+04  infeas =  0.000e+00 (0)
*  3000: obj =   4.734000000e+03  infeas =  0.000e+00 (0)
*  3500: obj =   4.410000000e+03  infeas =  0.000e+00 (0)
*  4000: obj =   3.729500000e+03  infeas =  0.000e+00 (0)
*  4500: obj =   3.465500000e+03  infeas =  8.327e-17 (0)
*  5000: obj =   3.161000000e+03  infeas =  0.000e+00 (0)
*  5500: obj =   3.035000000e+03  infeas =  2.712e-11 (0)
*  5977: obj =   2.961000000e+03  infeas =  0.000e+00 (0)
OPTIMAL LP SOLUTION FOUND
Integer optimization begins...
+  5977: mip =     not found yet >=              -inf        (1; 0)
+  5997: >>>>>   2.961000000e+03 >=   2.961000000e+03   0.0% (1; 0)
+  5997: mip =   2.961000000e+03 >=     tree is empty   0.0% (0; 1)
INTEGER OPTIMAL SOLUTION FOUND
Time used:   53.5 secs
Memory used: 572.4 Mb (600222575 bytes)
Distancia total: 2961.00000
P escolhidos , max 100
N0: 0	N1: 0	N2: 0	N3: 0	N4: 0	N5: 0	N6: 0	N7: 0	N8: 0	N9: 0	N10: 1	N11: 0	N12: 1	N13: 0	N14: 0	N15: 1	N16: 0	N17: 0	N18: 0	N19: 0	N20: 0	N21: 0	N22: 0	N23: 1	N24: 0	N25: 0	N26: 0	N27: 0	N28: 0	N29: 0	N30: 1	N31: 1	N32: 0	N33: 0	N34: 0	N35: 0	N36: 0	N37: 0	N38: 0	N39: 0	N40: 1	N41: 1	N42: 0	N43: 0	N44: 1	N45: 1	N46: 1	N47: 0	N48: 0	N49: 0	N50: 0	N51: 1	N52: 0	N53: 1	N54: 0	N55: 0	N56: 0	N57: 0	N58: 0	N59: 0	N60: 1	N61: 1	N62: 0	N63: 0	N64: 1	N65: 0	N66: 0	N67: 0	N68: 0	N69: 0	N70: 0	N71: 0	N72: 0	N73: 0	N74: 1	N75: 0	N76: 1	N77: 0	N78: 0	N79: 0	N80: 1	N81: 0	N82: 0	N83: 0	N84: 0	N85: 1	N86: 0	N87: 0	N88: 0	N89: 0	N90: 1	N91: 0	N92: 0	N93: 0	N94: 1	N95: 0	N96: 0	N97: 0	N98: 1	N99: 0	N100: 0	N101: 0	N102: 0	N103: 1	N104: 0	N105: 1	N106: 1	N107: 0	N108: 0	N109: 0	N110: 1	N111: 0	N112: 0	N113: 0	N114: 0	N115: 1	N116: 0	N117: 0	N118: 0	N119: 0	N120: 1	N121: 0	N122: 0	N123: 0	N124: 0	N125: 0	N126: 0	N127: 1	N128: 1	N129: 0	N130: 0	N131: 0	N132: 1	N133: 0	N134: 0	N135: 0	N136: 1	N137: 0	N138: 0	N139: 0	N140: 0	N141: 0	N142: 1	N143: 0	N144: 0	N145: 0	N146: 0	N147: 0	N148: 0	N149: 0	N150: 0	N151: 0	N152: 0	N153: 1	N154: 0	N155: 0	N156: 0	N157: 0	N158: 0	N159: 0	N160: 0	N161: 0	N162: 0	N163: 0	N164: 0	N165: 0	N166: 0	N167: 1	N168: 0	N169: 0	N170: 0	N171: 0	N172: 1	N173: 0	N174: 0	N175: 0	N176: 0	N177: 0	N178: 1	N179: 1	N180: 0	N181: 1	N182: 0	N183: 0	N184: 0	N185: 0	N186: 0	N187: 0	N188: 1	N189: 0	N190: 0	N191: 0	N192: 0	N193: 0	N194: 0	N195: 1	N196: 0	N197: 0	N198: 0	N199: 1	N200: 0	N201: 1	N202: 0	N203: 0	N204: 0	N205: 1	N206: 0	N207: 0	N208: 0	N209: 0	N210: 0	N211: 1	N212: 0	N213: 1	N214: 0	N215: 0	N216: 0	N217: 1	N218: 0	N219: 0	N220: 1	N221: 0	N222: 0	N223: 0	N224: 0	N225: 0	N226: 0	N227: 1	N228: 1	N229: 0	N230: 0	N231: 0	N232: 0	N233: 1	N234: 0	N235: 0	N236: 0	N237: 0	N238: 0	N239: 0	N240: 0	N241: 0	N242: 0	N243: 0	N244: 0	N245: 1	N246: 0	N247: 0	N248: 0	N249: 0	N250: 0	N251: 0	N252: 0	N253: 1	N254: 0	N255: 1	N256: 0	N257: 0	N258: 0	N259: 0	N260: 0	N261: 0	N262: 0	N263: 0	N264: 0	N265: 0	N266: 1	N267: 0	N268: 1	N269: 0	N270: 0	N271: 0	N272: 0	N273: 0	N274: 0	N275: 0	N276: 0	N277: 0	N278: 0	N279: 0	N280: 0	N281: 0	N282: 0	N283: 0	N284: 0	N285: 0	N286: 0	N287: 0	N288: 0	N289: 1	N290: 0	N291: 1	N292: 0	N293: 0	N294: 1	N295: 0	N296: 0	N297: 1	N298: 0	N299: 0	N300: 0	N301: 0	N302: 0	N303: 0	N304: 0	N305: 0	N306: 0	N307: 0	N308: 0	N309: 1	N310: 1	N311: 0	N312: 1	N313: 0	N314: 1	N315: 0	N316: 0	N317: 1	N318: 0	N319: 0	N320: 0	N321: 0	N322: 1	N323: 0	N324: 0	N325: 0	N326: 0	N327: 1	N328: 0	N329: 0	N330: 1	N331: 0	N332: 0	N333: 0	N334: 0	N335: 0	N336: 1	N337: 1	N338: 0	N339: 0	N340: 1	N341: 0	N342: 0	N343: 0	N344: 0	N345: 0	N346: 0	N347: 0	N348: 0	N349: 0	N350: 0	N351: 0	N352: 0	N353: 0	N354: 0	N355: 1	N356: 0	N357: 0	N358: 0	N359: 0	N360: 1	N361: 0	N362: 1	N363: 0	N364: 1	N365: 0	N366: 0	N367: 1	N368: 0	N369: 1	N370: 1	N371: 0	N372: 0	N373: 0	N374: 0	N375: 0	N376: 1	N377: 1	N378: 0	N379: 0	N380: 0	N381: 0	N382: 0	N383: 1	N384: 0	N385: 0	N386: 0	N387: 0	N388: 0	N389: 0	N390: 0	N391: 1	N392: 0	N393: 0	N394: 0	N395: 0	N396: 0	N397: 0	N398: 1	N399: 0	N400: 1	N401: 0	N402: 0	N403: 0	N404: 0	N405: 0	N406: 0	N407: 0	N408: 0	N409: 0	N410: 0	N411: 0	N412: 0	N413: 0	N414: 0	N415: 0	N416: 0	N417: 1	N418: 1	N419: 0	N420: 1	N421: 1	N422: 0	N423: 0	N424: 0	N425: 0	N426: 0	N427: 0	N428: 1	N429: 0	N430: 0	N431: 0	N432: 0	N433: 0	N434: 0	N435: 1	N436: 0	N437: 0	N438: 0	N439: 0	N440: 0	N441: 0	N442: 0	N443: 1	N444: 0	N445: 0	N446: 0	N447: 0	N448: 0	N449: 0	N450: 0	N451: 1	N452: 0	N453: 0	N454: 0	N455: 0	N456: 0	N457: 0	N458: 0	N459: 0	N460: 0	N461: 0	N462: 0	N463: 0	N464: 0	N465: 1	N466: 0	N467: 0	N468: 0	N469: 0	N470: 0	N471: 0	N472: 0	N473: 0	N474: 0	N475: 0	N476: 0	N477: 0	N478: 0	N479: 0	N480: 0	N481: 1	N482: 0	N483: 0	N484: 0	N485: 0	N486: 0	N487: 1	N488: 1	N489: 0	N490: 1	N491: 0	N492: 0	N493: 1	N494: 0	N495: 0	N496: 0	N497: 0	N498: 0	N499: 1	
Model has been successfully processed
