## RELATORIO GLPSOL LFSCHAUREN/RDLUGOKENSKI PARA pmed4.dat ##
!! Rodada 1 do pmed4. ++ Inicio: Dom Jun 21 12:51:30 BRT 2015
GLPSOL: GLPK LP/MIP Solver, v4.52
Parameter(s) specified in the command line:
 --model p-median.mod --data dat/pmed4.dat --tmlim 7200
Reading model section from p-median.mod...
35 lines were read
Reading data section from dat/pmed4.dat...
211 lines were read
Generating Total_Distance...
Generating P...
Generating Correct_Attendance...
Generating Correct_Attendance_2...
Model has been successfully generated
GLPK Integer Optimizer, v4.52
10102 rows, 10100 columns, 40000 non-zeros
10100 integer variables, all of which are binary
Preprocessing...
10101 rows, 10100 columns, 30100 non-zeros
10100 integer variables, all of which are binary
Scaling...
 A: min|aij| =  1.000e+00  max|aij| =  1.000e+00  ratio =  1.000e+00
Problem data seem to be well scaled
Constructing initial basis...
Size of triangular part is 10101
Solving LP relaxation...
GLPK Simplex Optimizer, v4.52
10101 rows, 10100 columns, 30100 non-zeros
      0: obj =   1.524000000e+04  infeas =  1.900e+01 (0)
*    19: obj =   1.524000000e+04  infeas =  0.000e+00 (0)
*   500: obj =   1.502500000e+04  infeas =  0.000e+00 (0)
*  1000: obj =   3.408000000e+03  infeas =  6.259e-14 (0)
*  1280: obj =   3.034000000e+03  infeas =  0.000e+00 (0)
OPTIMAL LP SOLUTION FOUND
Integer optimization begins...
+  1280: mip =     not found yet >=              -inf        (1; 0)
+  1283: >>>>>   3.034000000e+03 >=   3.034000000e+03   0.0% (1; 0)
+  1283: mip =   3.034000000e+03 >=     tree is empty   0.0% (0; 1)
INTEGER OPTIMAL SOLUTION FOUND
Time used:   0.5 secs
Memory used: 23.0 Mb (24155839 bytes)
Distancia total: 3034.00000
P escolhidos , max 20
N0: 1	N1: 0	N2: 0	N3: 0	N4: 0	N5: 1	N6: 0	N7: 1	N8: 0	N9: 1	N10: 0	N11: 0	N12: 1	N13: 0	N14: 0	N15: 0	N16: 0	N17: 0	N18: 0	N19: 0	N20: 0	N21: 1	N22: 0	N23: 0	N24: 0	N25: 1	N26: 0	N27: 0	N28: 0	N29: 0	N30: 0	N31: 0	N32: 0	N33: 1	N34: 0	N35: 0	N36: 0	N37: 1	N38: 0	N39: 0	N40: 0	N41: 0	N42: 0	N43: 0	N44: 0	N45: 0	N46: 0	N47: 0	N48: 0	N49: 0	N50: 1	N51: 0	N52: 0	N53: 0	N54: 1	N55: 0	N56: 0	N57: 0	N58: 0	N59: 1	N60: 0	N61: 0	N62: 0	N63: 0	N64: 0	N65: 1	N66: 0	N67: 0	N68: 0	N69: 0	N70: 0	N71: 1	N72: 0	N73: 0	N74: 0	N75: 0	N76: 1	N77: 0	N78: 0	N79: 0	N80: 0	N81: 0	N82: 1	N83: 0	N84: 0	N85: 0	N86: 1	N87: 0	N88: 0	N89: 0	N90: 1	N91: 0	N92: 1	N93: 0	N94: 0	N95: 1	N96: 0	N97: 0	N98: 0	N99: 0	
Model has been successfully processed
