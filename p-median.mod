# Modelagem de P Mediana Nao Capacitada
# Por Luis Fernando Schauren, Rodrigo Dlugokenski
# Otimizacao Combinatoria 2015-1 - Prof Marcus Ritt
# Ciencia da Computacao - Instituto de Informatica da UFRGS

set NODES;

param distance {NODES, NODES} >= 0;   # distancia
param pmed >= 0;
var x {NODES, NODES} binary;    # xij - quem eh atendido por quem
var y {NODES} binary;    # quem foram os escolhidos

minimize Total_Distance:
  sum {i in NODES, j in NODES} x[i,j] * distance[i,j];

#Total de P medianas
subject to P:
  sum {j in NODES} y[j] = pmed;

#Previne que um vertice seja atendido por um que nao eh mediana
subject to Correct_Attendance {i in NODES, j in NODES}:
  x[i,j] <= y[j];

#Cada vertice eh atendido por um dos p vertices que sao medianas
subject to Correct_Attendance_2 {i in NODES}:
  sum {j in NODES} x[i,j] = 1;


solve;

printf "Distancia total: %.5f\n",Total_Distance;
printf "P escolhidos , max %d\n",pmed;
printf {j in NODES} "%s: %d\t",j,y[j];
printf "\n";
end;
