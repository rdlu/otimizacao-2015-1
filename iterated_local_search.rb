#Handbook of Metaheuristics, p.369
class IteratedLocalSearch
  def initialize(state,iter_max,iter_max_no_improv)
    @state = state
    @iter_max = iter_max
    @iter_max_no_improv = iter_max_no_improv
    @initial_solution = state.random_state.local_search
    puts "Solucao Inicial: #{@initial_solution.total_cost}"
  end
  def search
    solution = @initial_solution
    iter = 0
    iter_no_improv = 0
    while(iter < @iter_max && iter_no_improv < @iter_max_no_improv)
      iter+=1
      solution_2 = solution.clone.perturba(1).local_search

      #criterio de aceitacao
      if(solution_2.total_cost < solution.total_cost)
        if $DEBUG
          puts "Melhora obtida #{solution_2.total_cost}"
        end
        solution = solution_2
        iter_no_improv = 0
      else
        #principal criterio de termino: numero de rodadas sem melhora
        iter_no_improv += 1
        if $DEBUG
          puts "Sem melhora ##{iter_no_improv}, iteracao ##{iter}"
        end
      end
    end
    puts "Total de iteracoes #{iter}"
    return solution
  end
end