# README #

Um script ruby que executa a metaheurística ILS em cima do problema das p medianas

Por Luis Fernando Schauren e Rodrigo Dlugokenski

Necessita do interpretador Ruby, em versão 2.0 ou superior.

Caso seu S.O. não tenha o Ruby 2.0+, aqui esta um guia de instalação como sugestão:

https://gorails.com/setup
Somente a seção "Installing Ruby" é necessária.