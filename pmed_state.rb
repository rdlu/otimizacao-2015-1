require 'set'

class PmedState
  def initialize(data)
    @data = data
    #custo do centro mais favoravel
    @costs = Array.new(data.node_count)
    #custo do 2o centro mais favoravel
    @costs_2nd = Array.new(data.node_count)
    #por quem eh atendido no caso mais favoravel
    @attended_by = Array.new(data.node_count)
    #por quem eh atendido no segundo caso mais favoravel
    @attended_2nd_by = Array.new(data.node_count)
    #conjunto de nos escolhidos para serem os distribuidores
    @p_medianas = Set.new
    @custo = Float::INFINITY
    random_state
  end

  def random_state
    @p_medianas.clear
    while @p_medianas.count < @data.p
      @p_medianas << rand(@data.node_count-1).to_i
    end

    calcula_estado
    self
  end

  def calcula_estado
    @custo = Float::INFINITY
    (0...nro_nodos).map do |i|
      #o centro de distribuicao com menor custo
      melhor_centro = melhores_centros(i)[0]
      @costs[i] = melhor_centro[:cost]
      @attended_by[i] = melhor_centro[:node]
      segundo_melhor_centro = melhores_centros(i)[1]
      @costs_2nd[i] = segundo_melhor_centro[:cost]
      @attended_2nd_by[i] = segundo_melhor_centro[:node]
    end

    @custo = calc_custo_total
    self
  end

  def nro_nodos
    @data.node_count
  end

  def edge_count
    @data.edge_count
  end

  def melhores_centros(cliente)
    @data.custos_ordenados(@p_medianas, cliente)
  end

  def calc_custo_total
    @costs.reduce :+
  end

  def custo_total
    (@custo == Float::INFINITY) ? calc_custo_total : @custo
  end

  def total_cost
    custo_total
  end

  def distancia_melhor_centro(cliente)
    @costs[cliente]
  end

  def melhor_centro(cliente)
    @attended_by[cliente]
  end

  def p_medianas
    @p_medianas
  end

  def distancia_segundo_melhor_centro(cliente)
    @costs_2nd[cliente]
  end

  def segundo_melhor_centro(cliente)
    @attended_2nd_by[cliente]
  end

  #baseado em Hansen and Mladenovic
  #para uso na busca local
  def encontrar_fabrica_remocao(candidato_insercao)
    gain = 0
    netloss = Hash.new
    @p_medianas.each do |f|
      netloss[f] = 0
    end

    (0...nro_nodos).map do |u|
      custo_caso_candidato_ok = @data.custos_ordenados([candidato_insercao], u)[0][:cost]
      if custo_caso_candidato_ok < distancia_melhor_centro(u)
        #temos aqui que o lucro do candidato eh bom o suficiente (menor individualmente, nao necessariamente global)
        gain += (distancia_melhor_centro(u) - custo_caso_candidato_ok)
      else
        #aqui temos certamente uma perda com a remocao
        netloss[melhor_centro(u)] += [custo_caso_candidato_ok, distancia_segundo_melhor_centro(u)].min - distancia_melhor_centro(u)
      end
    end
    candidato_remocao = netloss.min_by{ |x| x[1] }[0]
    lucro = gain - netloss[candidato_remocao]
    return {:to_remove => candidato_remocao, :to_insert => candidato_insercao, :profit => lucro}
  end

  #busca_local com best improvement
  def local_search
    candidatos = Hash.new
    #calcula para todos os candidatos a insercao e guarda o melhor para ser removido
    (0...nro_nodos).map do |candidato_insercao|
      if !@p_medianas.include?(candidato_insercao)
        #se nao estiver entre os ja escolhidos
        #tenta encontrar o melhor candidato (best improvement)
        candidatos[candidato_insercao] = encontrar_fabrica_remocao(candidato_insercao)
      end
    end
    #agora eu tenho os candidatos de rem/ins que oferecem o melhor lucro
    melhor = candidatos.max_by {|x| x[1][:profit]}

    if(melhor[1][:profit] > 0)
      @p_medianas.delete(melhor[1][:to_remove])
      @p_medianas << melhor[1][:to_insert]
      calcula_estado
    end

    #puts "Profit: #{melhor[1][:profit]}  | Custo novo: #{custo_total}"
    self
  end

  #troca aleatoriamente n das p-medianas sem passar pelo calculo de lucro
  def perturba(n)
    aux = @p_medianas.to_a
    #faz um embaralhamento na array de p_medianas e remove n
    aux2 = aux.shuffle!.shift(n)
    @p_medianas = aux.to_set
    while @p_medianas.count < @data.p
      novo_candidato = rand(@data.node_count-1).to_i
      if !aux2.include?(novo_candidato)
        @p_medianas << novo_candidato
      end
    end
    calcula_estado
    self
  end
end