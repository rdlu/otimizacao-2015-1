#! /usr/bin/ruby
# Por Luis Fernando Schauren, Rodrigo Dlugokenski
# Otimizacao Combinatoria 2014-2 - Prof Marcus Ritt
# Ciencia da Computacao - Instituto de Informatica da UFRGS


require 'matrix'


class PmedData
  #inicializa a leitura dos dados para matriz de adjacencias/custos
  def initialize(file)
    #leitura por estados:
    # inicial : Linha inicial
    # arestas : Demais linhas
    @estado = :inicial
    $stderr.puts "Lendo arquivo #{file}."
    begin
      File.foreach(file) do |line|
        line.strip!
        items = line.split(' ')
        case @estado
          when :inicial
            #INICIALIZACAO: distancia 0 para si mesmo, infinito para os demais
            @matriz_original = Matrix.build(items[0].to_i,items[0].to_i,){|row, col| row==col ? 0 : Float::INFINITY }
            @p = (items[2].to_i)
            @arestas = (items[1].to_i)
            @estado = :arestas
          when :arestas
            #repreenche a matriz com os custos lidos das linhas do arquivo
            @matriz_original.send(:[]=,items[0].to_i-1,items[1].to_i-1,items[2].to_i)
            @matriz_original.send(:[]=,items[1].to_i-1,items[0].to_i-1,items[2].to_i)
          else
            raise 'Estado nao previsto para leitura do arquivo'
        end
      end
    rescue Exception => e
      $stderr.puts "Erro ao ler arquivo: #{e.message}\n"
      $stderr.puts e.backtrace
    end
    $stderr.puts "Arquivo lido com sucesso. Vertices: #{@matriz_original.row_count}, P: #{@p}"

    calcula_distancias
    return self
  end

  #Floyd–Warshall algorithm
  def calcula_distancias
    $stderr.puts 'Preenchendo matriz de custos com Floyd–Warshall algorithm.'
    @matriz_custo = @matriz_original.clone
    for k in 0...(node_count)
      for i in 0...(node_count)
        for j in 0...(node_count)
          if custo(i,j) > custo(i,k) + custo(k,j)
            @matriz_custo.send(:[]=,i,j,custo(i,k) + custo(k,j))
          end
        end
      end
    end
  end

  def matriz_custo
    @matriz_custo
  end

  def node_count
    @matriz_custo.row_count
  end

  def edge_count
    @arestas
  end


  def matriz_adj
    @matriz_original
  end

  def vertice?(i,j)
    @matriz_original.element(i,j) < Float::INFINITY
  end

  def custo(i,j)
    @matriz_custo.element(i,j)
  end

  def custos_ordenados(nodos_origens,nodo_destino)
    custos = Array.new
    nodos_origens.each do |origem|
      custos << { :node => origem, :cost => custo(origem,nodo_destino) }
    end
    #operador <=> retorna -1,0,1 (esq menor),(iguais),(direita menor)
    custos.sort! do |a,b|
      a[:cost] <=> b[:cost]
    end
    custos
  end

  def print
      #imprime matriz para debug
      @matriz_custo.each_with_index do |e,r,c|
        puts "[#{r}][#{c}] = #{e}"
      end
  end

  def p
    @p
  end

end