#! /bin/bash
files=(4 8 12 16 20 24 28 32 36 40)

for i in "${files[@]}"
do
	file="pmed$i"
	echo "!! Rodando o algoritmo ILS com o arquivo data/$file.txt"
	echo "## RELATORIO ILS LFSCHAUREN/RDLUGOKENSKI PARA $file.txt ##" > reports/$file.txt
	for j in {1..5}
	do
   		echo "!! Rodada $j do $file. ++ Inicio: `date`"
   		echo "!! Rodada $j do $file. ++ Inicio: `date`" >> reports2/$file.txt
   		./run-pmed.rb -a data/$file.txt -n 1/4 -m 800 >> reports2/$file.txt
	done
done
