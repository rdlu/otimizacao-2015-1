#! /usr/bin/env ruby
# Por Luis Fernando Schauren, Rodrigo Dlugokenski
# Otimizacao Combinatoria 2015-1 - Prof Marcus Ritt
# Ciencia da Computacao - Instituto de Informatica da UFRGS

require './pmed_data'

if (ARGV[0])
  @data = PmedData.new(ARGV[0])
  $stderr.puts "Imprimindo dados glpk no stdout"
  begin
    puts "data;"
    puts "param pmed := #{@data.p};"
    puts ""
    puts "set NODES\t :="
    (0...(@data.node_count)).each { |i|
      puts "\tN#{i}"
    }
    puts ";"
    puts ""
    puts "param distance (tr)"
    line = "\t:"
    (0...(@data.node_count)).each { |i|
      line << "\tN#{i}"
    }
    puts line << "\t:="

    (0...(@data.node_count)).each { |i|
      line = "\tN#{i}"
      (0...(@data.node_count)).each { |j|
        line << "\t#{@data.custo(i, j)!=Float::INFINITY ? @data.custo(i, j) : 'Infinity'}"
      }
      puts line
    }
    puts ";"
    puts "end;"
    puts ""
  rescue Exception => e
    $stderr.puts "Erro geral: #{e.message} \n#{e.backtrace}"
  end
else
  $stderr.puts '--- USO DO COMANDO: ruby conversor.rb NOMEDOARQUIVO.TXT --- '
  $stderr.puts 'A saida do arquivo GLPK eh feita no STDOUT use redirecionamento para salvar em arquivo'
  $stderr.puts 'Exemplo: ruby conversor.rb origem/arq.txt > destino/dados.dat'
end